h3. Notes on r7 squares

h3. Existing squares code

!rollup.png!

Review branches: [levski|https://bitbucket.org/cenx-cf/levski/branches/compare/f%2FCD-40807-review%0Dlevski-1.9.13#diff], [plataea|https://bitbucket.org/cenx-cf/plataea/branches/compare/f%2FCD-40807-review%0Dplataea-1.9.12#diff]

Some of the existing squares code implement features outside the scope of the initial r7 squares requirements:

* derived metrics such as delivery and best-delivery
* availability calculations
* masked and unmasked rollups: a masked rollup ignores the samples received during a maintenance window
* performance exceptions

[CD-25122|https://cenx-cf.atlassian.net/browse/CD-25122] - squares issue on r5 in which queries on the input tables (performance_data) timed out because of inadequate partitioning. It should not happen with r7 squares because the input will be the Kafka topic or the short-term table (ttl is configured and week is in the partition key).

h3. Design process we used

# Propose a basic design that meets the requirements:
#* make a diagram showing components and their relationships (control, data flow, composition, etc.);
#* do not say anything about how each component will be implemented;
#** in-memory vs. on-disk;
#** location of a process (on Wildfly, on Spark, on the same machine, on different machines, etc).
# Enumerate the ways to implement the design;
#* it will be possible to implement some components in more than one way.
# Enumerate the advantages and disadvantages of each implementation option.
# Use the information produced by steps 2 and 3 to decide how each piece will be implemented.

h3. Writing a Spark job that reads from Kafka

To make the squares code independent of the short-term table:

* RDDs in Spark streaming live inside DStreams. These are sequences of RDDs.
* The number of cores allocated to the Spark app must be greater than the number of receivers.
* The function updateStateByKey may be useful for squares and TCAs: "The updateStateByKey operation allows you to maintain arbitrary state while continuously updating it with new information." (Spark streaming guide). It works on pair DStreams.
* Many of the functions in [PairDStreamFunctions|https://spark.apache.org/docs/2.0.2/api/scala/index.html#org.apache.spark.streaming.dstream.PairDStreamFunctions] accept a partitioner.
* Spark can read from Kafka through a Receiver or directly: "with directStream, Spark Streaming will create as many RDD partitions as there are Kafka partitions to consume, which will all read data from Kafka in parallel."
* Then the partitioning defined in the Kafka topic should be transferred to the Spark DStream.
* [KafkaUtils|https://spark.apache.org/docs/2.0.2/api/scala/index.html#org.apache.spark.streaming.kafka.KafkaUtils$] provides functions for creating direct streams.

[Spark streaming guide|https://spark.apache.org/docs/2.0.2/streaming-programming-guide.html]
[Spark/Kafka integration guide|https://spark.apache.org/docs/2.0.2/streaming-kafka-0-8-integration.html]
[Example|https://cwiki.apache.org/confluence/display/KAFKA/0.8.0+Producer+Example] of how to create a partitioned topic and write to it.

h3. Partitioning

A Spark job runs most efficiently when each node works in isolation. We want to reduce the crosstalk (shuffling) among nodes.

Notes from the [Heather Miller slides|http://heather.miller.am/teaching/cs212/slides/week20.pdf]:

* The default number of partitions is the number of cores on all executor nodes (slide 40).
* Custom partitioning is only possible on pair RDDs (slide 40).
* Be mindful of the number of elements a partitioner will assign to each partition.
* [RangePartitioner|https://spark.apache.org/docs/2.0.2/api/scala/index.html#org.apache.spark.RangePartitioner] can improve the distribution of elements across the nodes (slide 48).
* RangePartitioner expects a number of partitions and an RDD. It samples the RDD to determine the key ranges for each partition (slide 51).
* The result of partitionBy should be persisted. Otherwise, Spark re-partitions the RDD (shuffling) each time it is used (slide 51).
* Many operations on pair RDDs preserve partitioning: join, groupByKey, reduceByKey, mapValues, filter (slide 53). Any operation not listed on slide 53 will produce a result without a partitioner.

h3. Task list

From a conversation with Andrew:

# Write code that creates the input RDD by querying the short-term table
#* The partitions in the RDD must correspond to the partitions of the short-term table.
#* [Spark-Cassandra connector doc on partitioning|https://github.com/datastax/spark-cassandra-connector/blob/v2.0.1/doc/16_partitioning.md]
# Calculate min, max, avg
#* Use the input RDD created from the short-term table.
#* Test the code on the cluster.
# Join min, max, avg with topo attributes and generate daily rollup
#* Use result from task 2.
#* Get the topo attributes from the cenx-platform.
#* Save on the rolling daily (tracker) table.
# Setup rollup tables
#* Rolling daily (tracker), daily rollup, seven-day rollup, weekly rollup, monthly rollup.
# Generate weekly and monthly rollups, write csv
# Config validation
#* Short-term table ttl has to be greater than rolling daily frequency.

Dependencies:
2 -> 1, 3 -> 2, 3 -> 4, 5 -> 3, 5 -> 4

h3. Creation of RDD from the short-term table

By default, the cassandraTable method in the SparkContext will create an RDD that doesn't have a partitioner. This is bad because Spark may spread the data across the nodes in a random way. What we want is to have each Spark node process the local Cassandra data. In other words, we would like to have an RDD that maintains the partitioning of the Cassandra table.

The Spark-Cassandra connector provides a way to assign a partitioner to the RDD obtained from a Cassandra table \[1]. The method cassandraTable in SparkContext creates objects of class CassandraTableScanRDD, which contains the method keyBy.

The distribution of a table's data across the Cassandra nodes depends not only on the table's partition key, but also on the partitioner used for the table \[2], on the replication factor and on whether the cluster is using virtual-nodes \[3].

Cassandra will calculate a 128-bit number (a token) for each unique partition key. The set of tokens (all numbers from 0 to 2 raised to 128) is divided into segments. Each node in the cluster is responsible for one or more segments. The number of segments depends of the replication factor and on the virtual-nodes setting.

Quote from the Spark-Cassandra connector doc about the CassandraPartitioner created with keyBy: "The Partitioner uses a RowWriter like the saveToCassandra method to convert key data to a Cassandra token." \[4]

References:

\[1] [How to take advantage of Cassandra Partitioning in Spark|https://github.com/datastax/spark-cassandra-connector/blob/v2.0.1/doc/16_partitioning.md#how-to-take-advantage-of-cassandra-partitioning-in-spark]

\[2] [Partitioners in Cassandra|http://docs.datastax.com/en/cassandra/3.0/cassandra/architecture/archPartitionerAbout.html]

\[3] [Virtual nodes|http://docs.datastax.com/en/cassandra/3.0/cassandra/architecture/archDataDistributeVnodesUsing.html]

\[4] [Spark-Cassandra connector partitioning caveats|https://github.com/datastax/spark-cassandra-connector/blob/v2.0.1/doc/16_partitioning.md#caveats]

The article below has a keyBy example. Note that it applies a HashPartitioner after keyBy (does it replace the CassandraPartitioner?) and also caches the RDD.

https://techmagie.wordpress.com/2015/12/19/understanding-spark-partitioning/

h3. A test for showing that a Spark node is using data from a neighbor

Set up a small data set and determine the node where each element is located. Group the elements based on their location. This will result in a map.

* [Stack Overflow page|https://stackoverflow.com/questions/17201361/cassandra-how-to-identify-and-list-the-nodes-that-contain-a-particular-row-rep] showing a way to find which node contains a partition key:
\\
{code}
leonardo-nobregaMBP:~ leonardo.nobrega$ ssh deployer@cassandra01-amun.cenx.localnet docker exec -i cassandra1 nodetool getendpoints dev17cenx entity 10.75.14.129-laLoadInt
deployer@cassandra01-amun.cenx.localnet's password:
172.32.1.104
{code}
\\
{code}
leonardo-nobregaMBP:plataea leonardo.nobrega$ cqlsh -e "select token(id,idfamily,week) from leintest_analytics.vnf_data limit 1;"

 system.token(id, idfamily, week)
----------------------------------
              1231396800135031664

(1 rows)
leonardo-nobregaMBP:plataea leonardo.nobrega$ docker exec -i cassandra1 nodetool getendpoints leintest_analytics vnf_data 1231396800135031664
172.17.0.1
{code}

Load the data set onto an RDD then use [org.apache.spark.api.java.JavaPairRDD.mapPartitionsWithIndex|https://spark.apache.org/docs/2.0.2/api/scala/index.html#org.apache.spark.api.java.JavaPairRDD] to obtain a map of partition-index to elements-in-partition. Compare this map to the one from the previous step.

h3. Difficulties in writing generic code that loads an RDD with partitioner

This is about [CD-44658|https://cenx-cf.atlassian.net/browse/CD-44658].

Below is a function that creates a key object from a CassandraRow. The CassandraPartitioner requires a RowReaderFactory. The RowReader objects created by the factory must provide one such function.

{code}
(fn [row] (new scala.Tuple3
               (.getString row "id")
               (.getString row "idfamily")
               (.getLong row "week")))
{code}

It seems the only way to write a "new" expression that is independent of the number of columns in the partition key is by using a macro. It's not possible to "apply" new.

{code}
user> (apply new '(scala.Tuple3 1 2 3))
CompilerException java.lang.RuntimeException: Unable to resolve symbol: new in this context, compiling:(*cider-repl plataea*:718:7) 
user> (apply scala.Tuple3. '(1 2 3))
CompilerException java.lang.ClassNotFoundException: scala.Tuple3., compiling:(*cider-repl plataea*:720:7) 
user>
{code}

A macro that creates the read row function would need a list of partition key columns. But macros mapping on or concatenating to a sequence argument fail, if the argument is a reference instead of a literal.

{code}
user> (def a [1 2 3])
#'user/a
user> ;; this fails
user> (defmacro x [seq-arg] (list* '+ 0 seq-arg))
#'user/x
user> (x a)
IllegalArgumentException Don't know how to create ISeq from: clojure.lang.Symbol  clojure.lang.RT.seqFrom (RT.java:542)
user> ;; this works
user> (x [1 2 3])
6
user> ;; this also works
user> (defmacro y [seq-arg] (list '* 10 (list 'count seq-arg)))
#'user/y
user> (y a)
30
user> 
{code}

h3. Unit test template for code that uses Cassandra and Spark

This was the starting point for cenx.plataea.cassandra.vnf.rdd's tests.

{code}
(ns cenx.plataea.cassandra.empty-test
  (:require [cenx.plataea.cassandra :as cs]
            [cenx.plataea.cassandra.test :as ct :refer [*session*]]
            [cenx.prometheus.spark.context :as context]
            [cenx.prometheus.spark.test :as st :refer [*sc*]]
            [clojure.test :as t]
            [qbits.alia :as alia]
            [qbits.hayt :as hayt]
            [taoensso.timbre :as log]))

(t/use-fixtures :once
  (ct/cassandra-fixture :cass-host (or (System/getenv "CASSANDRA_HOST")
                                       "localhost")
                        :cass-prefix (or (System/getenv "CASSANDRA_PREFIX")
                                         "leintest_"))
  (st/spark-context-fixture :spark-app-name (str *ns*)
                            :spark-master (or (System/getenv "SPARK_MASTER")
                                              "local[*]")
                            :spark-jars (context/get-jar "plataea")
                            :spark-opts {"spark.cassandra.connection.host" (or (System/getenv "CASSANDRA_HOST")
                                                                               "localhost")}))

(defn clear-tables
  "Deletes test data."
  [session]
  (dorun (map #(alia/execute session %)
              [(hayt/use-keyspace (keyword (cs/analytics)))
               ;; TODO delete test data
               ])))

(t/deftest empty-test
  (try

    ;; TODO create test data

    (t/testing "something"
      (t/is true))

    (finally (clear-tables *session*))))
{code}

h3. Starting and stopping a Levski component on the repl

After seeing the start and stop functions outside Andrew's square component (on the [squares bootstrap PR|https://bitbucket.org/cenx-cf/levski/pull-requests/1009]), I wanted to know how to start and stop a component in which the functions are encapsulated:

(Note the absence of kafka-topic in the component after stopping it - this indicates the component is inactive.)

{code}
user=> (in-ns 'prod.init)
#object[clojure.lang.Namespace 0x17d4d914 "prod.init"]
prod.init=> (def system (update system :kafka-watcher (fn [c] (.stop c))))
17-Oct-10 14:57:38 wildfly_dc1 INFO [cenx.levski.kafka.core] - Stopping Kafka parser
#'prod.init/system
prod.init=> (-> prod.init/system :kafka-watcher keys)
(:analytics-config :cassandra-schema :channel-service :kafka-consumer)
prod.init=> (def system (update system :kafka-watcher (fn [c] (.start c))))
17-Oct-10 14:58:10 wildfly_dc1 INFO [cenx.levski.kafka.core] - Starting Kafka parser
#'prod.init/system
prod.init=> (-> prod.init/system :kafka-watcher keys)
(:analytics-config :cassandra-schema :channel-service :kafka-consumer :kafka-topic)
prod.init=>
{code}

h3. Javascript/Clojure example

Began working on [CD-44659|https://cenx-cf.atlassian.net/browse/CD-44659] by cobbling this together:

{code}
(let [js-min "function (a, b) { if (a < b) { return a; } return b; }"
      js-max "function (a, b) { if (a > b) { return a; } return b; }"
      eng (-> (javax.script.ScriptEngineManager.)
              (.getEngineByName "nashorn"))
      fn-min (.eval eng js-min)
      fn-max (.eval eng js-max)
      data [32.0 61.0 44.0 19.0 83.0]]
  (println (reduce #(.call fn-min nil (into-array [%1 %2])) 100.0 data))
  (println (reduce #(.call fn-max nil (into-array [%1 %2])) 0.0 data)))
{code}

h3. Finding the latest timestamp in the short-term table

!sample-too-late.png!

The squares processor runs periodically. Each run begins by executing a query on the Cassandra cluster to get the latest data.

One can write a cql query with a where clause that specifies a minimum timestamp: the query returns rows with a timestamp greater than this minimum.

The timestamp in the short-term table rows will generally be different from the time when the row is created. If the clocks in the Cassandra cluster and the network machine from which the sample comes had the same time, the time when the row is created would be greater than the time when the corresponding sample was generated, to account for the time the sample takes to travel from its source to the Cassandra cluster.

Suppose the latest time is defined to be the greatest value of the timestamp column in the short-term table. The squares processor starts and uses time T as the earliest time to get the samples from Cassandra. Suppose a sample generated at time T - 1 suffers a delay in the network that causes it to arrive after the squares processor finishes the run. The earliest time to be used in the next run will be greater that T. The next query will not return the sample, so the squares report will not take it into account.

h3. Checking the uniqueness of the ids in QA-AGW's analytics_report Solr collection

While working on [CD-44901|https://cenx-cf.atlassian.net/browse/CD-44901], I wanted to know the average size of a squares doc in Solr. The Solr web console informs the size of the collection (100 MB) and the total number of documents (3078). I had a guess that all documents had unique ids.

{code}
http://system01.qa-agw.cenx.localnet:8983/solr/analytics_report/select
?q=*:*
&rows=4000
&fl=id
&wt=csv
&indent=true
{code}

I saved the result of this query to a file and checked the number of ids with:

{{cat id-query.txt | sort | uniq | wc -l}}

The result was 3079 (there is one line with the attribute name).
